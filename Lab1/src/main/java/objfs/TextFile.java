package objfs;


public class TextFile extends FsNode {
    String data;

    public TextFile(String data) {
        this.data = data;
    }

    public String read() {
        return readonly(() -> data);
    }
}
